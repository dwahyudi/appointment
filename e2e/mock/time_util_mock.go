package mock

import "time"

type (
	mockTimeUtil struct {
	}
)

func (*mockTimeUtil) TimeNow() time.Time {
	return time.Date(2022, 9, 14, 12, 0, 0, 0, time.UTC)
}
