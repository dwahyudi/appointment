package mock

import "appointment/infra"

type (
	mockInfra struct {
		IDGen            infra.IDGen
		ScheduleDataPath string
		TimeUtil         infra.TimeUtil
	}
)

func (i *mockInfra) IDGenProvider() infra.IDGen {
	return i.IDGen
}

func (i *mockInfra) ScheduleDataPathProvider() string {
	return i.ScheduleDataPath
}

func (i *mockInfra) TimeProvider() infra.TimeUtil {
	return i.TimeUtil
}
