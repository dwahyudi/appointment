package mock

type (
	idGenMock struct {
	}
)

var count int

func (i *idGenMock) ULID() string {
	count += 1
	return i.ulidMocks()[count-1]
}

func (i *idGenMock) ulidMocks() []string {
	return []string{
		"01GD5B2XW2ATR70DSTWEG6MYSM",
		"01GD5B32FB2T0X3WYGX9TRVHAK",
		"01GD5B37HYCZ8FCKVFT1TY7WAK",
		"01GD5B3CCFXEV1SZD01WDWQ1Q2",
		"01GD5B521G3FG6Z6W7QXZP5AK0",
		"01GD5BM8RX1HSSMF53H9J8EC3X",
		"01GD5HT4W2JMCEQMMSNK1QH4QC",
	}
}
