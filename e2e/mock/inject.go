package mock

import (
	"appointment/infra"
	"sync"
)

var (
	mockInfraOnce sync.Once
	mockInfraInj  infra.Infra
)

func NewMockInfra() infra.Infra {
	mockInfraOnce.Do(func() {
		mockInfraInj = &mockInfra{
			IDGen:            &idGenMock{},
			ScheduleDataPath: "data/mock_data.csv",
			TimeUtil:         &mockTimeUtil{},
		}
	})
	return mockInfraInj
}
