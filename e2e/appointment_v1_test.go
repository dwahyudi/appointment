package e2e_test

import (
	"appointment/entity"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"testing"

	"github.com/go-resty/resty/v2"
	"github.com/stretchr/testify/assert"
)

func TestAppointments(t *testing.T) {
	cl := resty.New()

	// Create a new appointment
	requestBody := `
	{
    "coachee_name": "Badu",
    "from": 1663333675,
    "to": 1663340838
	}`

	resp, err := cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "coaches/C-01GD5B2XW2ATR70DSTWEG6MYSM/request_appointment"))
	if err != nil {
		log.Fatalln(err)
	}

	var coachSchedules entity.CoachSchedulesResponse
	err = json.Unmarshal(resp.Body(), &coachSchedules)
	if err != nil {
		log.Fatal(err)
	}

	assert.Equal(t, http.StatusCreated, resp.StatusCode())
	assert.Equal(t, `{"new_appointment_id":"A-01GD5BM8RX1HSSMF53H9J8EC3X"}`, string(resp.Body()))

	// Create another new appointment
	requestBody = `
	{
    "coachee_name": "Bima",
    "from": 1663340839,
    "to": 1663341839
	}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "coaches/C-01GD5B2XW2ATR70DSTWEG6MYSM/request_appointment"))
	if err != nil {
		log.Fatalln(err)
	}

	err = json.Unmarshal(resp.Body(), &coachSchedules)
	if err != nil {
		log.Fatal(err)
	}

	assert.Equal(t, http.StatusCreated, resp.StatusCode())
	assert.Equal(t, `{"new_appointment_id":"A-01GD5HT4W2JMCEQMMSNK1QH4QC"}`, string(resp.Body()))

	// Try to create another new appointment, but conflict with existing appointment
	requestBody = `
		{
			"coachee_name": "Tono",
			"from": 1663333775,
			"to": 1663340938
		}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "coaches/C-01GD5B2XW2ATR70DSTWEG6MYSM/request_appointment"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusUnprocessableEntity, resp.StatusCode())

	// Try to create another new appointment, but conflict with existing appointment
	requestBody = `
		{
			"coachee_name": "Eko",
			"from": 1663333475,
			"to": 1663340738
		}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "coaches/C-01GD5B2XW2ATR70DSTWEG6MYSM/request_appointment"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusUnprocessableEntity, resp.StatusCode())

	// Check appointments by a coach ID
	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Get(fmt.Sprintf("%s/%s", V1BaseURL, "coaches/C-01GD5B2XW2ATR70DSTWEG6MYSM/appointments"))
	if err != nil {
		log.Fatalln(err)
	}

	var appnsResponse []entity.AppointmentResponse
	err = json.Unmarshal(resp.Body(), &appnsResponse)
	if err != nil {
		log.Fatal(err)
	}

	assert.Equal(t, http.StatusCreated, resp.StatusCode())
	assert.Equal(t, `[{"id":"A-01GD5BM8RX1HSSMF53H9J8EC3X","coachee_name":"Badu","from":"2022-09-16 13:07:55 +0000 UTC","to":"2022-09-16 15:07:18 +0000 UTC"},{"id":"A-01GD5HT4W2JMCEQMMSNK1QH4QC","coachee_name":"Bima","from":"2022-09-16 15:07:19 +0000 UTC","to":"2022-09-16 15:23:59 +0000 UTC"}]`, string(resp.Body()))

	// Cancel appointment by an appointment ID
	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Patch(fmt.Sprintf("%s/%s", V1BaseURL, "appointments/A-01GD5BM8RX1HSSMF53H9J8EC3X/cancel"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())

	// Cancel non existent appointment by an appointment ID
	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Patch(fmt.Sprintf("%s/%s", V1BaseURL, "appointments/A-01GD79GC1AFDYQR7G7RV12EJ8E/cancel"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusNotFound, resp.StatusCode())

	// Check appointments by a coach ID
	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Get(fmt.Sprintf("%s/%s", V1BaseURL, "coaches/C-01GD5B2XW2ATR70DSTWEG6MYSM/appointments"))
	if err != nil {
		log.Fatalln(err)
	}

	err = json.Unmarshal(resp.Body(), &appnsResponse)
	if err != nil {
		log.Fatal(err)
	}

	assert.Equal(t, http.StatusCreated, resp.StatusCode())
	assert.Equal(t, `[{"id":"A-01GD5HT4W2JMCEQMMSNK1QH4QC","coachee_name":"Bima","from":"2022-09-16 15:07:19 +0000 UTC","to":"2022-09-16 15:23:59 +0000 UTC"}]`, string(resp.Body()))
}

func TestRequestAppointmentsValidation(t *testing.T) {
	cl := resty.New()

	// Try to create a new appointment, from is later than to
	requestBody := `
		{
			"coachee_name": "Tono",
			"from": 1663340938,
			"to": 1663333775
		}`

	resp, err := cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "coaches/C-01GD5B3CCFXEV1SZD01WDWQ1Q2/request_appointment"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusUnprocessableEntity, resp.StatusCode())

	// Try to create a new appointment, but from is earlier than time mock (e2e/mock/time_util_mock.go)
	requestBody = `
		{
			"coachee_name": "Tono",
			"from": 1663233775,
			"to": 1663340938
		}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "coaches/C-01GD5B3CCFXEV1SZD01WDWQ1Q2/request_appointment"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusUnprocessableEntity, resp.StatusCode())
}
