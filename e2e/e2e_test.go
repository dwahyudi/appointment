package e2e_test

import (
	"appointment/e2e/mock"
	"appointment/restful"
	"log"
	"os"
	"testing"
	"time"
)

const (
	V1BaseURL = "http://localhost:7777/v1"
)

func TestMain(m *testing.M) {
	log.Print("Initiating End to End tests...")

	log.Print("Setting up End to End tests...")
	prepared := make(chan (bool))
	go func() {
		infraInject := mock.NewMockInfra()

		restful.InjectNewServer(infraInject).Run(prepared)
	}()

	<-prepared
	log.Print("Setting up Complete...")

	time.Sleep(2 * time.Duration(time.Second))

	log.Print("Running tests...")
	code := m.Run()

	os.Exit(code)
}
