package e2e_test

import (
	"appointment/entity"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"testing"

	"github.com/go-resty/resty/v2"
	"github.com/stretchr/testify/assert"
)

func TestGetCoaches(t *testing.T) {
	cl := resty.New()

	resp, err := cl.R().
		SetHeader("content-type", "application/json").
		Get(fmt.Sprintf("%s/%s", V1BaseURL, "coaches"))
	if err != nil {
		log.Fatalln(err)
	}

	var coaches []entity.Coach
	err = json.Unmarshal(resp.Body(), &coaches)
	if err != nil {
		log.Fatal(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())

	expectedResponse := []entity.Coach{
		{
			CoachID: "C-01GD5B37HYCZ8FCKVFT1TY7WAK",
			Name:    "Fulan bin Fulan",
		},
		{
			CoachID: "C-01GD5B3CCFXEV1SZD01WDWQ1Q2",
			Name:    "Mr. Healthy",
		},
		{
			CoachID: "C-01GD5B521G3FG6Z6W7QXZP5AK0",
			Name:    "Budiman",
		},
		{
			CoachID: "C-01GD5B2XW2ATR70DSTWEG6MYSM",
			Name:    "John Doe",
		},
		{
			CoachID: "C-01GD5B32FB2T0X3WYGX9TRVHAK",
			Name:    "Aaron Roger",
		},
	}

	assert.ElementsMatch(t, expectedResponse, coaches)
}

func TestGetCoachSchedules(t *testing.T) {
	cl := resty.New()

	resp, err := cl.R().
		SetHeader("content-type", "application/json").
		Get(fmt.Sprintf("%s/%s", V1BaseURL, "coaches/C-01GD5B2XW2ATR70DSTWEG6MYSM/schedules"))
	if err != nil {
		log.Fatalln(err)
	}

	var coachSchedules entity.CoachSchedulesResponse
	err = json.Unmarshal(resp.Body(), &coachSchedules)
	if err != nil {
		log.Fatal(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())

	expectedResponse := entity.CoachSchedulesResponse{
		Name: "John Doe",
		Schedules: []entity.ScheduleResponse{
			{
				Day:      "Monday",
				Timezone: "America/North_Dakota/New_Salem",
				From:     "09:00",
				To:       "17:30",
			},
			{
				Day:      "Tuesday",
				Timezone: "America/North_Dakota/New_Salem",
				From:     "08:00",
				To:       "16:00",
			},
			{
				Day:      "Thursday",
				Timezone: "America/North_Dakota/New_Salem",
				From:     "09:00",
				To:       "16:00",
			},
			{
				Day:      "Friday",
				Timezone: "America/North_Dakota/New_Salem",
				From:     "07:00",
				To:       "14:00",
			},
		},
	}

	assert.Equal(t, expectedResponse, coachSchedules)
}

func TestGetNotFoundCoach(t *testing.T) {
	cl := resty.New()

	resp, err := cl.R().
		SetHeader("content-type", "application/json").
		Get(fmt.Sprintf("%s/%s", V1BaseURL, "coaches/C-01GD78WQGCS5RBTRQK6TQ5DBP1/schedules"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusNotFound, resp.StatusCode())
}
