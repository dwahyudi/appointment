package infra

type (
	Infra interface {
		// PG()
		IDGenProvider() IDGen
		ScheduleDataPathProvider() string
		TimeProvider() TimeUtil
	}

	infra struct {
		IDGen            IDGen
		ScheduleDataPath string
		TimeUtil         TimeUtil
	}
)

func (i *infra) IDGenProvider() IDGen {
	return i.IDGen
}

func (i *infra) ScheduleDataPathProvider() string {
	return i.ScheduleDataPath
}

func (i *infra) TimeProvider() TimeUtil {
	return i.TimeUtil
}
