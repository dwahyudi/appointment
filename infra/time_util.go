package infra

import "time"

type (
	TimeUtil interface {
		TimeNow() time.Time
	}

	timeUtil struct {
	}
)

func (t *timeUtil) TimeNow() time.Time {
	return time.Now()
}
