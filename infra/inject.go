package infra

import "sync"

var (
	infraOnce sync.Once
	infraInj  Infra
)

func NewInfra() Infra {
	infraOnce.Do(func() {
		infraInj = &infra{
			IDGen:            &idGen{},
			ScheduleDataPath: "data/data.csv",
			TimeUtil:         &timeUtil{},
		}
	})
	return infraInj
}
