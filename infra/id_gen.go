package infra

import "github.com/oklog/ulid/v2"

type (
	IDGen interface {
		ULID() string
	}

	idGen struct {
	}
)

func (i *idGen) ULID() string {
	return ulid.Make().String()
}
