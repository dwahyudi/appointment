# Build the Binary

Please make sure we have `go` binary in the `PATH`. Minimum version Go: 1.12

Run this command:

`make build`

Run e2e tests:

`make e2e`

Run API server:

`make api`