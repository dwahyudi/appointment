package schedulerepo

import (
	"appointment/infra"
	"sync"
)

var (
	scheduleRepoOnce sync.Once
	scheduleRepoInj  ScheduleRepo
)

// func InjectNewPgScheduleRepo(infra infra.Infra) {
// }

func InjectNewMemoryScheduleRepo(infra infra.Infra) ScheduleRepo {
	scheduleRepoOnce.Do(func() {
		schedules, coaches := buildScheduleMemory(infra)
		scheduleRepoInj = &schedule{
			ScheduleMemory: schedules,
			Coaches:        coaches,
		}
	})

	return scheduleRepoInj
}
