package schedulerepo

import (
	"appointment/entity"
	"context"
)

type (
	ScheduleRepo interface {
		GetAvailableCoaches(ctx context.Context) ([]entity.Coach, error)
		GetSchedulesByCoachID(ctx context.Context, coachID entity.CoachID) (entity.CoachSchedule, error)
		IsAvailable(ctx context.Context, appn entity.Appointment) (bool, error)
	}
)
