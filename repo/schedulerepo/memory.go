package schedulerepo

import (
	"appointment/entity"
	"context"
	"errors"
	"time"
)

type (
	schedule struct {
		ScheduleMemory map[entity.CoachID]entity.CoachSchedule
		Coaches        []entity.Coach
	}
)

func (s *schedule) GetSchedulesByCoachID(
	ctx context.Context, coachID entity.CoachID,
) (entity.CoachSchedule, error) {
	coachSchedules, ok := s.ScheduleMemory[coachID]
	if !ok {
		return entity.CoachSchedule{}, entity.ErrNoCoachFound
	}
	return coachSchedules, nil
}

func (s *schedule) GetAvailableCoaches(ctx context.Context) ([]entity.Coach, error) {
	return s.Coaches, nil
}

func (s *schedule) IsAvailable(
	ctx context.Context, appn entity.Appointment,
) (bool, error) {
	appnFromTime := time.Unix(appn.From, 0)
	appnToTime := time.Unix(appn.To, 0)

	valid := false

	coachSchedule, ok := s.ScheduleMemory[appn.CoachID]
	if !ok {
		return false, errors.New("coach not found")
	}

	for _, eachSchedule := range coachSchedule.Schedules {
		loc, err := time.LoadLocation(eachSchedule.Timezone)
		if err != nil {
			return false, err
		}

		fromTimeInCoachTimeZone := appnFromTime.In(loc)
		toTimeInCoachTimeZone := appnToTime.In(loc)

		if eachSchedule.Weekday != time.Weekday(fromTimeInCoachTimeZone.Weekday()) {
			continue
		}

		year, month, day := fromTimeInCoachTimeZone.Date()

		scheduleStart := time.Date(year, month, day, eachSchedule.DayHourFrom.Hour, eachSchedule.DayHourFrom.Minute, 0, 0, loc)
		scheduleEnd := time.Date(year, month, day, eachSchedule.DayHourTo.Hour, eachSchedule.DayHourTo.Minute, 0, 0, loc)

		if fromTimeInCoachTimeZone.After(scheduleStart) && toTimeInCoachTimeZone.Before(scheduleEnd) {
			valid = true
			break
		}
	}

	return valid, nil
}

func (s *schedule) sampleSchedules(
	ctx context.Context,
) (map[entity.CoachID]entity.CoachSchedule, error) {
	return s.ScheduleMemory, nil
}
