package schedulerepo

import (
	"appointment/entity"
	"appointment/infra"
	"appointment/service/timesvc"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"strings"
	"sync"
	"time"
)

func buildScheduleMemory(infra infra.Infra) (map[entity.CoachID]entity.CoachSchedule, []entity.Coach) {
	file, err := os.Open(infra.ScheduleDataPathProvider())
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	schedules := make(map[entity.CoachID]entity.CoachSchedule)
	coachesTemp := make(map[string]entity.CoachID)

	csvReader := csv.NewReader(file)
	rowNum := 0
	for {
		row, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}

		rowNum += 1

		// skip header
		if rowNum == 1 {
			continue
		}

		schedules = processCSVRow(infra, row, schedules, coachesTemp)
	}

	coaches := make([]entity.Coach, 0)
	for coachName, coachID := range coachesTemp {
		coaches = append(coaches, entity.Coach{
			CoachID: coachID, Name: coachName,
		})
	}

	return schedules, coaches
}

func processCSVRow(
	infra infra.Infra, cell []string,
	schedules map[entity.CoachID]entity.CoachSchedule, coachesTemp map[string]entity.CoachID,
) map[entity.CoachID]entity.CoachSchedule {
	var (
		name       = cell[0]
		timezone   = cell[1]
		weekdayStr = cell[2]
		from       = strings.TrimSpace(cell[3])
		to         = strings.TrimSpace(cell[4])
	)

	weekday, ok := timesvc.WeekdaysMap()[weekdayStr]
	if !ok {
		log.Fatal("unknown weekday")
	}

	fromTime, err := time.Parse(time.Kitchen, from)
	if err != nil {
		log.Fatal(err)
	}

	toTime, err := time.Parse(time.Kitchen, to)
	if err != nil {
		log.Fatal(err)
	}

	// validate timezone
	r := regexp.MustCompile(`(\(.*\)) (.*)`)
	matches := r.FindStringSubmatch(timezone)
	_, err = time.LoadLocation(matches[2])
	if err != nil {
		log.Fatal(err)
	}

	newSchedule := entity.DayTimeRange{
		Timezone:    matches[2],
		Weekday:     weekday,
		DayHourFrom: entity.DayHour{Hour: fromTime.Hour(), Minute: fromTime.Minute()},
		DayHourTo:   entity.DayHour{Hour: toTime.Hour(), Minute: toTime.Minute()},
	}

	handleNewSchedule(infra, schedules, coachesTemp, name, newSchedule)

	return schedules
}

func handleNewSchedule(
	infra infra.Infra, schedules map[entity.CoachID]entity.CoachSchedule,
	coachesTemp map[string]entity.CoachID, name string, newSchedule entity.DayTimeRange,
) {
	existingCoachID, ok := coachesTemp[name]
	if ok {
		existingCoachSchedule := schedules[existingCoachID]

		handleExistingCoach(infra, existingCoachSchedule, newSchedule, schedules, existingCoachID)
	} else {
		newCoachID := fmt.Sprintf("C-%s", infra.IDGenProvider().ULID())

		coachesTemp[name] = entity.CoachID(newCoachID)
		handleNewCoach(infra, schedules, entity.CoachID(newCoachID), newSchedule, name)
	}
}

func handleNewCoach(infra infra.Infra, schedules map[entity.CoachID]entity.CoachSchedule, coachID entity.CoachID, newSchedule entity.DayTimeRange, name string) {
	schedules[coachID] = entity.CoachSchedule{
		Coach:     entity.Coach{CoachID: entity.CoachID(coachID), Name: name},
		Schedules: []entity.DayTimeRange{newSchedule},
		Mutex:     &sync.Mutex{},
	}
}

func handleExistingCoach(infra infra.Infra, existingCoachSchedule entity.CoachSchedule, newSchedule entity.DayTimeRange, schedules map[entity.CoachID]entity.CoachSchedule, name entity.CoachID) {
	newSchedules := append(existingCoachSchedule.Schedules, newSchedule)
	existingCoachSchedule.Schedules = newSchedules
	schedules[name] = existingCoachSchedule
}
