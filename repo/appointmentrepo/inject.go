package appointmentrepo

import (
	"appointment/infra"
	"sync"
)

var (
	appnRepoOnce sync.Once
	appnRepoInj  AppointmentRepo
)

func InjectNewAppointmentMemoryRepo(infra infra.Infra) AppointmentRepo {
	appnRepoOnce.Do(func() {
		appnRepoInj = &memoryAppointment{
			infra: infra,
		}
	})
	return appnRepoInj
}
