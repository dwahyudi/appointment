package appointmentrepo

import (
	"appointment/entity"
	"appointment/infra"
	"context"
	"fmt"
)

type (
	memoryAppointment struct {
		appointmentsMemory []entity.Appointment
		infra              infra.Infra
	}
)

func (a *memoryAppointment) Create(ctx context.Context, appn entity.Appointment) (entity.AppointmentID, error) {
	newID := fmt.Sprintf("A-%s", a.infra.IDGenProvider().ULID())
	a.appointmentsMemory = append(a.appointmentsMemory, entity.Appointment{
		ID:          entity.AppointmentID(newID),
		CoachID:     appn.CoachID,
		From:        appn.From,
		To:          appn.To,
		CoacheeName: appn.CoacheeName,
	})

	return entity.AppointmentID(newID), nil
}

func (a *memoryAppointment) Cancel(ctx context.Context, ID entity.AppointmentID) error {
	var index int

	found := false
	for i, appn := range a.appointmentsMemory {
		if appn.ID == ID {
			found = true
			index = i
		}
	}

	if !found {
		return entity.ErrNoScheduleFound
	}

	a.appointmentsMemory = append(
		a.appointmentsMemory[:index],
		a.appointmentsMemory[index+1:]...,
	)

	return nil
}

func (a *memoryAppointment) IsFreeSchedule(ctx context.Context, appnRequest entity.Appointment) (bool, error) {
	isFree := true

	coachAppointments, err := a.GetAppointmentsByCoachID(ctx, appnRequest.CoachID)
	if err != nil {
		return false, nil
	}

	for _, existingCoachAppn := range coachAppointments {
		if appnRequest.From >= existingCoachAppn.From && appnRequest.From <= existingCoachAppn.To {
			return false, nil
		}

		if appnRequest.To >= existingCoachAppn.From && appnRequest.To <= existingCoachAppn.To {
			return false, nil
		}
	}

	return isFree, nil
}

func (a *memoryAppointment) GetAppointmentsByCoachID(ctx context.Context, coachID entity.CoachID) ([]entity.Appointment, error) {
	coachAppointments := make([]entity.Appointment, 0)
	for _, existingAppn := range a.appointmentsMemory {
		if coachID == existingAppn.CoachID {
			coachAppointments = append(coachAppointments, existingAppn)
		}
	}

	return coachAppointments, nil
}
