package appointmentrepo

import (
	"appointment/entity"
	"context"
)

type (
	AppointmentRepo interface {
		Create(ctx context.Context, appn entity.Appointment) (entity.AppointmentID, error)
		Cancel(ctx context.Context, ID entity.AppointmentID) error
		IsFreeSchedule(ctx context.Context, appnRequest entity.Appointment) (bool, error)
		GetAppointmentsByCoachID(ctx context.Context, coachID entity.CoachID) ([]entity.Appointment, error)
	}
)
