package appointmentdomainrepo

import (
	"appointment/infra"
	"appointment/repo/appointmentrepo"
	"appointment/repo/schedulerepo"
	"sync"
)

var (
	appnDomainRepoOnce sync.Once
	appnDomainRepoInj  AppointmentDomainRepo
)

func InjectNewAppointmentDomainRepo(infra infra.Infra) AppointmentDomainRepo {
	appnDomainRepoOnce.Do(func() {
		appnDomainRepoInj = &appointmentDomainMemory{
			appnRepo:     appointmentrepo.InjectNewAppointmentMemoryRepo(infra),
			scheduleRepo: schedulerepo.InjectNewMemoryScheduleRepo(infra),
		}
	})

	return appnDomainRepoInj
}
