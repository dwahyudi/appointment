package appointmentdomainrepo

import (
	"appointment/entity"
	"appointment/repo/appointmentrepo"
	"appointment/repo/schedulerepo"
	"context"
	"errors"
)

type (
	appointmentDomainMemory struct {
		appnRepo     appointmentrepo.AppointmentRepo
		scheduleRepo schedulerepo.ScheduleRepo
	}
)

func (a *appointmentDomainMemory) RequestAppointment(
	ctx context.Context, appn entity.Appointment,
) (entity.AppointmentID, error) {
	var newAppointmentID entity.AppointmentID

	available, err := a.scheduleRepo.IsAvailable(ctx, appn)
	if err != nil {
		return "", err
	}
	if !available {
		return "", errors.New("schedule not available")
	}

	free, err := a.appnRepo.IsFreeSchedule(ctx, appn)
	if err != nil {
		return "", err
	}
	if !free {
		return "", errors.New("schedule not available")
	}

	newAppointmentID, err = a.appnRepo.Create(ctx, appn)
	if err != nil {
		return "", err
	}

	return newAppointmentID, nil
}
