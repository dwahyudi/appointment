package appointmentdomainrepo

import (
	"appointment/entity"
	"context"
)

type (
	AppointmentDomainRepo interface {
		RequestAppointment(ctx context.Context, appn entity.Appointment) (entity.AppointmentID, error)
	}
)
