package main

import (
	"appointment/infra"
	"appointment/restful"
	"os"
)

func main() {
	args := os.Args

	switch args[0] {
	case "api":
		runRESTfulAPI()
	default:
		runRESTfulAPI()
	}
}

func runRESTfulAPI() {
	infraInject := infra.NewInfra()

	restful.InjectNewServer(infraInject).Run(nil)
}
