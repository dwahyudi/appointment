package entity

type (
	CoachSchedulesResponse struct {
		Name      string             `json:"name"`
		Schedules []ScheduleResponse `json:"schedules"`
	}

	ScheduleResponse struct {
		Day      string `json:"day"`
		Timezone string `json:"timezone"`
		From     string `json:"from"`
		To       string `json:"to"`
	}

	AppointmentResponse struct {
		ID          string `json:"id"`
		CoacheeName string `json:"coachee_name"`
		From        string `json:"from"`
		To          string `json:"to"`
	}
)
