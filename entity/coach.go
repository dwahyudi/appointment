package entity

import "sync"

type CoachID string

type (
	Coach struct {
		CoachID CoachID `json:"coach_id"`
		Name    string  `json:"name"`
	}

	CoachSchedule struct {
		Coach     Coach
		Schedules []DayTimeRange

		Mutex *sync.Mutex
	}
)
