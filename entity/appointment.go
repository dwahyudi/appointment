package entity

type AppointmentID string

type (
	Appointment struct {
		ID          AppointmentID
		CoachID     CoachID
		CoacheeName string
		From        int64 // unix timestamp
		To          int64 // unix timestamp
	}
)
