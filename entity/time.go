package entity

import "time"

type Weekday int

type (
	DayHour struct {
		Hour   int
		Minute int
	}

	DayTimeRange struct {
		Weekday     time.Weekday
		Timezone    string
		DayHourFrom DayHour
		DayHourTo   DayHour
	}
)
