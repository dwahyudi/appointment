package entity

import "errors"

var (
	ErrNoCoachFound    = errors.New("no coach found")
	ErrNoScheduleFound = errors.New("no schedule found")
)
