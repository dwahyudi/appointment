package timesvc

import (
	"time"
)

func WeekdaysMap() map[string]time.Weekday {
	return map[string]time.Weekday{
		"Monday":    time.Monday,
		"Sunday":    time.Sunday,
		"Tuesday":   time.Tuesday,
		"Wednesday": time.Wednesday,
		"Thursday":  time.Thursday,
		"Friday":    time.Friday,
		"Saturday":  time.Saturday,
	}
}
