package appointmentdomainservice

import (
	"appointment/entity"
	"appointment/repo/appointmentdomainrepo"
	"appointment/repo/appointmentrepo"
	"context"
)

type (
	AppointmentDomainService interface {
		RequestAppointment(
			ctx context.Context, appn entity.Appointment,
		) (entity.AppointmentID, error)
		GetAppointmentsByCoachID(
			ctx context.Context, coachID entity.CoachID,
		) ([]entity.Appointment, error)
		Cancel(ctx context.Context, ID entity.AppointmentID) error
	}

	appointmentDomainService struct {
		appointmentDomainRepo appointmentdomainrepo.AppointmentDomainRepo
		appointmentRepo       appointmentrepo.AppointmentRepo
	}
)

func (a *appointmentDomainService) RequestAppointment(
	ctx context.Context, appn entity.Appointment,
) (entity.AppointmentID, error) {
	newAppointmentDomainID, err := a.appointmentDomainRepo.RequestAppointment(ctx, appn)
	if err != nil {
		return "", err
	}

	return newAppointmentDomainID, nil
}

func (a *appointmentDomainService) GetAppointmentsByCoachID(
	ctx context.Context, coachID entity.CoachID,
) ([]entity.Appointment, error) {
	appns, err := a.appointmentRepo.GetAppointmentsByCoachID(ctx, coachID)
	if err != nil {
		return nil, err
	}

	return appns, nil
}

func (a *appointmentDomainService) Cancel(ctx context.Context, ID entity.AppointmentID) error {
	err := a.appointmentRepo.Cancel(ctx, ID)
	if err != nil {
		return err
	}

	return nil
}
