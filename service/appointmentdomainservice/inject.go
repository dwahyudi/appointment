package appointmentdomainservice

import (
	"appointment/infra"
	"appointment/repo/appointmentdomainrepo"
	"appointment/repo/appointmentrepo"
	"sync"
)

var (
	appnDomainServiceOnce sync.Once
	appnDomainInj         AppointmentDomainService
)

func InjectNewAppointmentDomainService(infra infra.Infra) AppointmentDomainService {
	appnDomainServiceOnce.Do(func() {
		appnDomainInj = &appointmentDomainService{
			appointmentDomainRepo: appointmentdomainrepo.InjectNewAppointmentDomainRepo(infra),
			appointmentRepo:       appointmentrepo.InjectNewAppointmentMemoryRepo(infra),
		}
	})

	return appnDomainInj
}
