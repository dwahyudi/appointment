package scheduleservice

import (
	"appointment/entity"
	"appointment/repo/schedulerepo"
	"context"
)

type (
	ScheduleService interface {
		GetAvailableCoaches(ctx context.Context) ([]entity.Coach, error)
		GetSchedulesByCoachID(ctx context.Context, coachID entity.CoachID) (entity.CoachSchedule, error)
	}

	scheduleService struct {
		scheduleRepo schedulerepo.ScheduleRepo
	}
)

func (s *scheduleService) GetSchedulesByCoachID(ctx context.Context, coachID entity.CoachID) (entity.CoachSchedule, error) {
	schedules, err := s.scheduleRepo.GetSchedulesByCoachID(ctx, coachID)
	if err != nil {
		return entity.CoachSchedule{}, err
	}

	return schedules, nil
}

func (s *scheduleService) GetAvailableCoaches(ctx context.Context) ([]entity.Coach, error) {
	coaches, err := s.scheduleRepo.GetAvailableCoaches(ctx)
	if err != nil {
		return nil, err
	}

	return coaches, nil
}
