package scheduleservice

import (
	"appointment/infra"
	"appointment/repo/schedulerepo"
	"sync"
)

var (
	scheduleServiceOnce sync.Once
	scheduleServiceInj  ScheduleService
)

func InjectNewScheduleService(infra infra.Infra) ScheduleService {
	scheduleServiceOnce.Do(func() {
		scheduleServiceInj = &scheduleService{
			scheduleRepo: schedulerepo.InjectNewMemoryScheduleRepo(infra),
		}
	})

	return scheduleServiceInj
}
