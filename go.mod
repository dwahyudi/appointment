module appointment

go 1.19

require github.com/oklog/ulid/v2 v2.1.0

require (
	github.com/go-chi/chi/v5 v5.0.7
	github.com/go-resty/resty/v2 v2.7.0
	github.com/stretchr/testify v1.8.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/net v0.0.0-20211029224645-99673261e6eb // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
