package restful

import "appointment/infra"

func InjectNewServer(infra infra.Infra) RestfulServer {
	return &restfulServer{
		infra: infra,
	}
}
