package appointmentrestful

import (
	"appointment/infra"
	"appointment/service/appointmentdomainservice"
	"sync"
)

var (
	appnRestfulOnce sync.Once
	appnRestfulInj  AppointmentRestful
)

func InjectNewAppointmentRestful(infra infra.Infra) AppointmentRestful {
	appnRestfulOnce.Do(func() {
		appnRestfulInj = &appointmentRestful{
			appnDomainService: appointmentdomainservice.InjectNewAppointmentDomainService(infra),
			infra:             infra,
		}
	})

	return appnRestfulInj
}
