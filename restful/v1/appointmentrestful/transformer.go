package appointmentrestful

import (
	"appointment/entity"
	"context"
	"time"
)

func transformAppointment(ctx context.Context, appn entity.Appointment) entity.AppointmentResponse {
	return entity.AppointmentResponse{
		ID:          string(appn.ID),
		CoacheeName: appn.CoacheeName,
		From:        time.Unix(appn.From, 0).UTC().String(),
		To:          time.Unix(appn.To, 0).UTC().String(),
	}
}
