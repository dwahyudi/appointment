package appointmentrestful

import (
	"errors"
	"time"
)

type (
	RequestForm struct {
		CoacheeName string `json:"coachee_name"`
		From        int64  `json:"from"`
		To          int64  `json:"to"`
	}
)

func (r *RequestForm) Validate(timeNow time.Time) error {
	if r.To <= r.From {
		return errors.New("to time must be later than from time")
	}

	if r.From <= timeNow.Unix() {
		return errors.New("invalid from time")
	}

	return nil
}
