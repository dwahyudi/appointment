package appointmentrestful

import (
	"appointment/entity"
	"appointment/infra"
	rc "appointment/restful/restfulcommon"
	"appointment/service/appointmentdomainservice"
	"encoding/json"
	"errors"
	"net/http"

	"github.com/go-chi/chi/v5"
)

type (
	AppointmentRestful interface {
		RequestAppointment(w http.ResponseWriter, r *http.Request)
		GetAppointmentsByCoachID(w http.ResponseWriter, r *http.Request)
		Cancel(w http.ResponseWriter, r *http.Request)
	}

	appointmentRestful struct {
		appnDomainService appointmentdomainservice.AppointmentDomainService
		infra             infra.Infra
	}
)

func (a *appointmentRestful) RequestAppointment(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	coachID := chi.URLParam(r, "coachID")

	var form RequestForm
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&form)
	if err != nil {
		rc.SetResponse(w, http.StatusBadRequest, "bad request")
		return
	}

	err = form.Validate(a.infra.TimeProvider().TimeNow())
	if err != nil {
		rc.SetResponse(w, http.StatusUnprocessableEntity, "bad request")
		return
	}

	newAppointmentID, err := a.appnDomainService.RequestAppointment(ctx, entity.Appointment{
		CoachID: entity.CoachID(coachID), From: int64(form.From), To: int64(form.To), CoacheeName: form.CoacheeName,
	})
	if err != nil {
		rc.SetResponse(w, http.StatusUnprocessableEntity, err.Error())
		return
	}

	response := struct {
		NewAppointmentID string `json:"new_appointment_id"`
	}{
		NewAppointmentID: string(newAppointmentID),
	}

	rc.SetJSONResponse(w, http.StatusCreated, response)
}

func (a *appointmentRestful) GetAppointmentsByCoachID(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	coachID := chi.URLParam(r, "coachID")

	appns, err := a.appnDomainService.GetAppointmentsByCoachID(ctx, entity.CoachID(coachID))
	if err != nil {
		rc.SetResponse(w, http.StatusUnprocessableEntity, err.Error())
		return
	}

	appnsResponses := make([]entity.AppointmentResponse, 0)
	for _, appn := range appns {
		appnsResponses = append(appnsResponses, transformAppointment(ctx, appn))
	}

	rc.SetJSONResponse(w, http.StatusCreated, appnsResponses)
}

func (a *appointmentRestful) Cancel(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	appointmentID := chi.URLParam(r, "appointmentID")

	err := a.appnDomainService.Cancel(ctx, entity.AppointmentID(appointmentID))
	if err != nil {
		if errors.Is(err, entity.ErrNoScheduleFound) {
			rc.SetResponse(w, http.StatusNotFound, err.Error())
		}

		rc.SetResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	rc.SetResponse(w, http.StatusOK, "")
}
