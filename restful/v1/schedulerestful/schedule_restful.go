package schedulerestful

import (
	"appointment/entity"
	rc "appointment/restful/restfulcommon"
	"appointment/service/scheduleservice"
	"errors"
	"net/http"

	"github.com/go-chi/chi/v5"
)

type (
	ScheduleRestful interface {
		GetAvailableCoaches(w http.ResponseWriter, r *http.Request)
		GetSchedulesByCoachID(w http.ResponseWriter, r *http.Request)
	}

	scheduleRestful struct {
		scheduleService scheduleservice.ScheduleService
	}
)

func (s *scheduleRestful) GetAvailableCoaches(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	coaches, err := s.scheduleService.GetAvailableCoaches(ctx)
	if err != nil {
		rc.SetResponse(w, http.StatusUnprocessableEntity, err.Error())
		return
	}

	rc.SetJSONResponse(w, http.StatusOK, coaches)
}

func (s *scheduleRestful) GetSchedulesByCoachID(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	coachID := chi.URLParam(r, "coachID")

	schedules, err := s.scheduleService.GetSchedulesByCoachID(ctx, entity.CoachID(coachID))
	if err != nil {
		if errors.Is(err, entity.ErrNoCoachFound) {
			rc.SetResponse(w, http.StatusNotFound, err.Error())
		}

		rc.SetResponse(w, http.StatusInternalServerError, err.Error())
		return
	}

	schedulesResponse := transformCoachSchedules(schedules)

	rc.SetJSONResponse(w, http.StatusOK, schedulesResponse)
}
