package schedulerestful

import (
	"appointment/infra"
	"appointment/service/scheduleservice"
	"sync"
)

var (
	scheduleRestfulOnce sync.Once
	scheduleRestfulInj  ScheduleRestful
)

func InjectNewScheduleRestful(infra infra.Infra) ScheduleRestful {
	scheduleRestfulOnce.Do(func() {
		scheduleRestfulInj = &scheduleRestful{
			scheduleService: scheduleservice.InjectNewScheduleService(infra),
		}
	})

	return scheduleRestfulInj
}
