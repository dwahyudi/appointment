package schedulerestful

import (
	"appointment/entity"
	"fmt"
)

func transformCoachSchedules(schedules entity.CoachSchedule) entity.CoachSchedulesResponse {
	schedulesResponse := make([]entity.ScheduleResponse, 0)
	for _, schedule := range schedules.Schedules {
		schedulesResponse = append(schedulesResponse,
			entity.ScheduleResponse{
				Day:      schedule.Weekday.String(),
				Timezone: schedule.Timezone,
				From:     fmt.Sprintf("%02d:%02d", schedule.DayHourFrom.Hour, schedule.DayHourFrom.Minute),
				To:       fmt.Sprintf("%02d:%02d", schedule.DayHourTo.Hour, schedule.DayHourTo.Minute),
			},
		)
	}

	return entity.CoachSchedulesResponse{
		Name:      schedules.Coach.Name,
		Schedules: schedulesResponse,
	}
}
