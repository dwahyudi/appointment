package restful

import (
	"appointment/infra"
	"appointment/restful/v1/appointmentrestful"
	"appointment/restful/v1/schedulerestful"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

type (
	RestfulServer interface {
		Run(prepared chan (bool))
	}

	restfulServer struct {
		infra infra.Infra
	}
)

func (rs *restfulServer) Run(prepared chan (bool)) {
	var (
		r     = chi.NewRouter()
		infra = rs.infra

		scheduleRestful = schedulerestful.InjectNewScheduleRestful(infra)
		appnRestful     = appointmentrestful.InjectNewAppointmentRestful(infra)
	)

	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.StripSlashes)

	r.Get("/health", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("ok"))
	})

	r.Route("/v1", func(v1Route chi.Router) {
		v1Route.Route("/coaches", func(coachesRoute chi.Router) {
			coachesRoute.Get("/", scheduleRestful.GetAvailableCoaches)
			coachesRoute.Get("/{coachID}/schedules", scheduleRestful.GetSchedulesByCoachID)
			coachesRoute.Post("/{coachID}/request_appointment", appnRestful.RequestAppointment)
			coachesRoute.Get("/{coachID}/appointments", appnRestful.GetAppointmentsByCoachID)
		})

		v1Route.Route("/appointments", func(appnRouter chi.Router) {
			appnRouter.Patch("/{appointmentID}/cancel", appnRestful.Cancel)
		})
	})

	if prepared != nil {
		prepared <- true
	}
	http.ListenAndServe(":7777", r)
}
